import { readFileSync } from 'node:fs';
import { join } from 'node:path';

import { Reader } from '@maxmind/geoip2-node';
import httpErrorHandler from '@middy/http-error-handler';

import type { ValidatedEventAPIGatewayProxyEvent } from '@libs/api-gateway';
import { formatJSONResponse } from '@libs/api-gateway';
import { handlerPath } from '@libs/handler-resolver';
import { middyfy } from '@libs/lambda';

const getDB = () => {
  const dbPath = handlerPath(join(process.cwd(), 'data/GeoLite2-Country.mmdb'));

  return Reader.openBuffer(readFileSync(dbPath));
};

const country: ValidatedEventAPIGatewayProxyEvent<void> = async (event) => {
  const ip = event.requestContext.identity.sourceIp;
  const response = getDB().country(ip);
  const isoCode = response.country?.isoCode;

  if (isoCode) {
    return formatJSONResponse({
      isoCode,
    });
  }

  throw new Error('unknown_country');
};

export const main = middyfy(country)
  .use(httpErrorHandler());
