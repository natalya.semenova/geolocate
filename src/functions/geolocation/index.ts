import { handlerPath } from '@libs/handler-resolver';

export const getCountry = {
  handler: `${handlerPath(__dirname)}/handler.main`,
  events: [
    {
      http: {
        method: 'get',
        path: 'country',
      },
    },
  ],
};
